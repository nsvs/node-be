const express = require('express');
const cors = require('cors')
const app = express();
const port = 3000;

const login = require('./login');
const authorization = require('./middleware/authorization');
const encode = require('./routes/encode');

app.use(cors())
app.use(express.json());

app.post('/login', login);

app.get('/encode', authorization, encode);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = app
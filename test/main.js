const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const server = require('../app');

describe('Routes', () => {
    describe('/login', () => {
        it('should return 401 if invalid user credetials are submitted', async () => {
            const email = 'optimus@autobots.com';
            const password = 'invalidPassword';
            const res = await chai.request(server).post('/login').send({ email, password });

            expect(res.status).to.equal(401);
            expect(res.text).to.equal('Login failed.');
        });

        it('should return 200 and token if valid user credetials are submitted', async () => {
            const email = 'optimus.prime@autobots.com';
            const password = 'validPassword1234!';
            const res = await chai.request(server).post('/login').send({ email, password });

            expect(res.status).to.equal(200);
            expect(res.body.token).to.equal('xyz0987654321');
        });
    });
    describe('/encode', () => {
        it('should return 401 if invalid token is provided in header', async () => {
            const res = await chai.request(server).get('/encode').set('authorization', 'Bearer 123').query({ value: 'AACC' });
            expect(res.status).to.equal(401);
            expect(res.body.value).to.equal(undefined);
        });

        it('should return 200 and encoded string from value AACC if valid token is provided', async () => {
            const res = await chai.request(server).get('/encode').set('authorization', 'Bearer xyz0987654321').query({ value: 'AACC' });

            expect(res.status).to.equal(200);
            expect(res.body.value).to.equal('A2C2');
        });

        it('should return 200 and encoded string from value ABBCCCDF', async () => {
            const res = await chai
                .request(server)
                .get('/encode')
                .set('authorization', 'Bearer xyz0987654321')
                .query({ value: 'ABBCCCDFF' });

            expect(res.status).to.equal(200);
            expect(res.body.value).to.equal('A1B2C3D1F2');
        });

        it('should return 200 and encoded string from value A', async () => {
            const res = await chai.request(server).get('/encode').set('authorization', 'Bearer xyz0987654321').query({ value: 'A' });

            expect(res.status).to.equal(200);
            expect(res.body.value).to.equal('A1');
        });
    });
});

function encodeString(input) {
    const charArray = (input && input.split('')) || [];
    let encodedString = '';
    let occurenceCount = 0;
    let alphaToInsert = charArray[0];
    const lastAlphaIndex = charArray.length - 1;

    charArray.forEach((alpha, index) => {
        // Check if we're on last array member
        if (index && index === lastAlphaIndex) {
            // Check if last array member is equal to previous
            if (alphaToInsert === charArray[lastAlphaIndex]) {
                encodedString += `${alphaToInsert}${occurenceCount + 2}`;
            } else {
                // Print out previous and last one
                encodedString += `${alphaToInsert}${occurenceCount + 1}`;
                encodedString += `${alpha}1`;
            }

            return;
        }

        // Check if prev alpha is equal to current one
        if (alphaToInsert === alpha && index !== lastAlphaIndex) {
            if (index) occurenceCount++;

            return;
        }

        // We have a new alpha, print prev
        encodedString += `${alphaToInsert}${occurenceCount + 1}`;

        alphaToInsert = alpha;

        occurenceCount = 0;
    });

    return encodedString;
}

// Alternative implementation of encoder
function encodeStringAlternative(input) {
    return input.split('').reduce((acc, value) => {
        if (acc.length === 0 || acc[acc.length - 1].key !== value)
            acc.push({ key: value, count: 1 });
        else
            acc[acc.length - 1].count++

        return acc;
    }, []).map(x => x.key + x.count).join('')
}

module.exports = function encode(req, res, next) {
    const stringToEncode = req.query.value;
    const encodedString = encodeString(stringToEncode);

    return res.send({ value: encodedString });
};

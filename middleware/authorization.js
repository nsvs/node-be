module.exports = function authorization(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token !== 'xyz0987654321') {
        return res.sendStatus(401);
    }

    next();
};
